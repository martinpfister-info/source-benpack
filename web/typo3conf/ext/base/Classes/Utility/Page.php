<?php
namespace Martinpfister\Base\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 sozialinfo.ch
 *  All rights reserved
 ***************************************************************/

/**
 * This class delivers little helpers connected to Page.
 *
 * @author Michal Cygankiewicz <m.cygankiewicz@amtsolution.pl>
 */

class Page
{
    /**
     * Return id of current page.
     *
     * @return int
     */
    public static function getCurrentPageUid()
    {
        return (int)$GLOBALS['TSFE']->id;
    }
}
