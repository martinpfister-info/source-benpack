//root namespace
var soz = soz || {};

soz.sijobs = {

    config: {
        publishButton: $("#publish-button"),
        endPublishing: $("#end_time_date"),
        startPublishing : $('#startPublishing'),
        endPublish : $('#endPublishing'),
        alertinfo : $('.alert-info')
    },

    setEndPublishing : function(){
        this.config.startPublishing.datepicker({
            language: 'de',
            format: 'dd.mm.yyyy',
            autoclose: true,
            todayHighlight: true
        });
        this.config.startPublishing.on("changeDate", function (e) {
            var publicationDays = soz.sijobs.getPublishDays();
            var startDate = new Date(e.date);
            startDate.setDate(startDate.getDate());
            var endDate = new Date(e.date);
            endDate.setDate(endDate.getDate() + publicationDays);

            soz.sijobs.config.endPublish.datepicker('setStartDate', startDate);
            soz.sijobs.config.endPublish.datepicker('setEndDate',  endDate);
            soz.sijobs.config.endPublish.datepicker('update', endDate);
            soz.sijobs.config.endPublish.datepicker({
                language: 'de',
                format: 'dd.mm.yyyy',
                autoclose: true,
                todayHighlight: true
            });
            soz.sijobs.updatePublishButtonText(startDate);

        });
    },
    updatePublishButtonText: function (startDate) {
        if(!startDate){
            startDate = soz.sijobs.config.startPublishing.datepicker('getDate');
        }

        var today = new Date();
        var immediateLabel = this.config.publishButton.find('span:first-child');
        var futureLabel = this.config.publishButton.find('span:last-child');
        if(startDate.getTime() > today.getTime()) {
            if(!immediateLabel.hasClass('hidden')){
                immediateLabel.toggleClass('hidden');
            }
            if(futureLabel.hasClass('hidden')){
                futureLabel.toggleClass('hidden');
            }
            futureLabel.find('em').html( soz.sijobs.config.startPublishing.datepicker('getFormattedDate'));
        }else{
            if(immediateLabel.hasClass('hidden')){
                immediateLabel.toggleClass('hidden');
            }
            if(!futureLabel.hasClass('hidden')){
                futureLabel.toggleClass('hidden');
            }
        }
    },
    init: function() {
        if ($('#startPublishing').length > 0) {
            this.setEndPublishing();
        }
        if(!isNaN(soz.sijobs.config.startPublishing.datepicker('getDate'))) {
            soz.sijobs.updatePublishButtonText();
        }
    },
    getPublishDays : function () {
        var MAXDAYSPOSITION = 5;
        if(this.config.alertinfo.length){
            return parseInt($.trim(this.config.alertinfo.text()).split(' ')[MAXDAYSPOSITION]);
        }
    }
};
