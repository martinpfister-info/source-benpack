<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

# Adding pageTSConfig
$pageTSConfig = "";
foreach (['BackendLayouts.ts', 'Page.ts'] as $fileName) {
    $pageTSConfig .= GeneralUtility::getUrl(
        GeneralUtility::getFileAbsFileName(
            'EXT:' . $_EXTKEY . '/Resources/Private/TsConfig/' . $fileName
        )
    );
}
ExtensionManagementUtility::addPageTSConfig($pageTSConfig);

# Adding UserTsConfig
$userTSConfigAbsFileName = GeneralUtility::getFileAbsFileName(
    'EXT:' . $_EXTKEY . '/Resources/Private/TsConfig/User.ts'
);
ExtensionManagementUtility::addUserTSConfig(GeneralUtility::getUrl($userTSConfigAbsFileName));

#
# Include TypoScript Setup automatically (upon Extension install)
#
ExtensionManagementUtility::addTypoScriptSetup(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'. $_EXTKEY .'/Configuration/TypoScript/setup.ts">'
);
ExtensionManagementUtility::addTypoScriptConstants(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'. $_EXTKEY .'/Configuration/TypoScript/constants.ts">'
);
