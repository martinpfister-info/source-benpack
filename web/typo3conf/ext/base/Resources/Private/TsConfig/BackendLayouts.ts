mod {
    web_layout {
        BackendLayouts {

            SingleColumn {
                title = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.SingleColumn
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.mainColumn
                                        colPos = 12
                                        colspan = 12
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:base/Resources/Public/Backend/Icons/Layouts/SingleColumn.png
            }
            TwoColumns {
                title = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.TwoColumns
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    2 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.mainColumn
                                        colPos = 16
                                        colspan = 6
                                    }
                                    3 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.rightColumn
                                        colPos = 26
                                        colspan = 6
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:base/Resources/Public/Backend/Icons/Layouts/TwoColumns.png
            }
            FourColumns {
                title = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.FourColumns
                icon = EXT:base/Resources/Public/Backend/Icons/Layouts/FourColumns.png
                config {
                    backend_layout {
                        colCount  = 4
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.firstColumn
                                        colPos = 1
                                    }
                                    2 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.secondColumn
                                        colPos = 2
                                    }
                                    3 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.thirdColumn
                                        colPos = 3
                                    }
                                    4 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.fourthColumn
                                        colPos = 4
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ComingSoon {
                title = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.ComingSoon
                icon = EXT:base/Resources/Public/Backend/Icons/Layouts/ComingSoon.png
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.mainColumn
                                        colPos = 12
                                        colspan = 12
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
