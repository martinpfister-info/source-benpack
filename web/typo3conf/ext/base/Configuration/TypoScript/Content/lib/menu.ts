#-------------------------------------------------------------------------------
#    Menu
#-------------------------------------------------------------------------------
lib.menu = COA
lib.menu {
    10  = HMENU
    10  {
        entryLevel = 0

        1 = TMENU
        1 {
            wrap = <ul class="nav navbar-nav">|</ul>
            target = _top
            NO {
                wrapItemAndSub = <li>|</li>
            }

            ACT < .NO
            ACT = 1
            ACT.wrapItemAndSub = <li class="active">|</li>

            CUR < .ACT
            CUR = 1
            CUR {
                # Selected is used by mmenu to open automatically the current page
                wrapItemAndSub  = <li class="selected">|</li>
            }

            SPC = 1
            SPC.before.dataWrap = <li class="separator">&mdash;</li>|
            SPC.doNotShowLink = 1
        }

        2 = TMENU
        2 {
            expAll = 0
            wrap = <ul class="nav">|</ul>

            NO {
                wrapItemAndSub = <li>|<span /></li>
            }
            ACT < .NO
            ACT = 1
            ACT.wrapItemAndSub = <li class="active">|<span /></li>

            CUR < .NO
            CUR = 1
            CUR {
                wrapItemAndSub = <li class="selected">|<span /></li>
            }
        }

        3 = TMENU
        3 {
            expAll = 0
            wrap = <ul class="nav">|</ul>

            NO {
                wrapItemAndSub = <li>|</li>
            }
            ACT < .NO
            ACT = 1
            ACT.wrapItemAndSub = <li class="active">|</li>

            CUR < .ACT
            CUR = 1
            CUR {
                wrapItemAndSub = <li class="selected">|</li>
            }
        }
    }

    20 = COA
    20.wrap = <nav id="si-menu-mobile"><ul class="nav">|</ul></nav>
    20.10 < .10
    20.10 {
        # mobile navigation is slightly different
        wrap =
        1.expAll = 1
        1.wrap =
        1.CUR.stdWrap.wrap >
        2.expAll = 1
        2.CUR.stdWrap.wrap >
        3.expAll = 1
        3.CUR.stdWrap.wrap >
    }
    20 >
}