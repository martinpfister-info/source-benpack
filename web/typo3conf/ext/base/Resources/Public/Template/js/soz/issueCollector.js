/**
 * temporary include of the jira issue collector. @todo remove before live-going.
 */
soz.issueCollector = {
    init: function () {
        jQuery.ajax({
            url: "https://sozialinfo.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-4s2hov/b/1/3d70dff4c40bd20e976d5936642e2171/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=de-DE&collectorId=6b42b888",
            type: "get",
            cache: true,
            dataType: "script"
        });
    }
};
