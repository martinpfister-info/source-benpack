//root namespace
var soz = soz || {};

/**
 * make table of class sortTable sortable
 * use <th data-sort="int">int</th>
 * convert date to int <td data-sort-value="672537600">April 25, 1991</td>
 *
 * @type {{init: soz.checkAllCheckboxes.init}}
 */
soz.sortTable = {

    config: {
        sortasc: '.sorting-asc',
        sortdesc: '.sorting-desc'
    },


    init: function () {
        $(".sortTable").stupidtable();

        $(this.config.sortasc).append('<i class="si-icon-angle-up>"');
        $(this.config.sortdesc).append('<i class="si-icon-angle-down>"');
    }


};