//root namespace
var soz = soz || {};

/**
 * loads up the ckeditor only on specified textarea's
 * to use it, add your textarea to the config.elements array
 *
 * first param: the name of the textarea
 * second param: the desired height of the textarea
 */
soz.frontendEditor = {

    config: {
        elements: [
            ['tx_sieducation_educationlists[education][description]', 400],
            ['tx_sijobs_joboffers[jobOffer][description]', 400],
            ['tx_sijobs_joboffers[jobOffer][furtherInformations]', 100],
            ['tx_sijobs_joboffers[jobOffer][applicationTo]', 100],
            ['tx_sijobs_jobapplication[jobApplication][description]', 400],
            ['tx_sijobs_jobapplication[jobApplication][applicationTo]', 100]
        ]
    },

    init: function () {
        $.each(this.config.elements , function( index, element ) {
            if ($('textarea[name="' + element[0] + '"]').length > 0) {
                CKEDITOR.replace(element[0], {
                    height: element[1]
                });
            }
        });
    }
};
