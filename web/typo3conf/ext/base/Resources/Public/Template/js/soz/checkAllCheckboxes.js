//root namespace
var soz = soz || {};

/**
 * give ability for user to check, uncheck all button in selected fieldset.
 * If you want to set things to select all checkbox in current div you need create items like:
 * <f:form.checkbox id="checkAll{youindividualName}" /> where {youindividualName} is string what you like
 * important part is only to start id with "checkAll"
 *
 */
soz.checkAllCheckboxes = {
    init: function () {
        $('[id^=checkAll]').click(function (event) {
            var checkboxes = $(this).closest('fieldset').find('input');
            if ($(this).prop("checked")) {
                checkboxes.prop('checked', 'checked');
            } else {
                checkboxes.prop('checked', '');
            }
        });
    }
};
