module.exports = function(grunt) {

    require( 'load-grunt-tasks' )( grunt );

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        // Adds vendor prefixes to given stylesheets.
        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')({browsers: ['last 2 version']}),
                    require('cssnano')()
                ]
            },
            dist: {
                src: ['dist/css/app.css']
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            options: {
                sourceMap: false,
                sourceMapEmbed: true,
                sourceMapContents: false,
                includePaths: ['bower_components/bootstrap-sass/assets/stylesheets',
                               'bower_components/bootstrap']
            },
            dist: {
                files: {
                    'dist/css/app.css': 'sass/app.scss'
                }
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'bower_components/jquery/dist/',
                        src: ['**'],
                        dest:'dist/js/frameworks/jquery'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'bower_components/bootstrap-sass/assets/javascripts/',
                        src: ['bootstrap.min.js'],
                        dest:'dist/js/frameworks/bootstrap'
                    },
                    // copies DIN fonts from src to dist
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'fonts/base',
                        src: ['*'],
                        dest:'dist/fonts'
                    }
                ]
            }
        },

        //concatenate js source files into one file
        concat: {
            dist: {
                // the files to concatenate
               src:grunt.file.readJSON('js/jsfiles.json').src,
                // the location of the resulting JS file
                dest: 'dist/js/app.js'
            }
        },

        // File change watcher
        watch: {
            grunt: {
                files: ['Gruntfile.js']
            },
            sass: {
                files: 'sass/**/*.scss',
                tasks: ['buildCSS']
            },
            javascript: {
                files: 'js/src/**/*.js',
                tasks: ['concat']
            }
        }
    });

    // Define tasks
    grunt.registerTask('compileSass', ['sass', 'postcss']);
    grunt.registerTask('buildCSS', ['compileSass']);
    grunt.registerTask('build', ['buildCSS', 'copy', 'concat']);

    // Define default task
    grunt.registerTask('default', ['build', 'watch']);
};
