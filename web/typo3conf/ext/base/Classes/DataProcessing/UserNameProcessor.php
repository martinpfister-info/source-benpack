<?php
namespace Martinpfister\Base\DataProcessing;

/***************************************************************
*  Copyright notice
*
*  (c) 2017 sozialinfo.ch
*  All rights reserved
***************************************************************/

use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * This class contains a data processer that can be used with any view based
 * on FLUIDTEMPLATE. It adds two new variables:
 * - bool isUserLoggedIn
 * - string userName (if user is logged in)
 *
 * @author Dmitry Dulepov <support@snowflake.ch>
 */
class UserNameProcessor implements DataProcessorInterface
{
    /** @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController */
    protected $tsfe;

    /**
     * Creates the instance of the class.
     */
    public function __construct()
    {
        $this->tsfe = $GLOBALS['TSFE'];
    }

    /**
     * Process content object data
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        if (!$this->tsfe->loginUser) {
            $processedData['isUserLoggedIn'] = false;
            $processedData['userName'] = '';
        } else {
            $processedData['isUserLoggedIn'] = true;
            if ($this->tsfe->fe_user->user['first_name']) {
                $processedData['userName'] = $this->tsfe->fe_user->user['first_name'] .
                    ' ' .
                    $this->tsfe->fe_user->user['last_name'];
            } else {
                $processedData['userName'] = $this->tsfe->fe_user->user['username'];
            }
        }
        return $processedData;
    }
}
