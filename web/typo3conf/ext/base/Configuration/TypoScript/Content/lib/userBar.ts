lib.userBar = COA_INT
lib.userBar.10 = FLUIDTEMPLATE
lib.userBar.10 {
    file = EXT:base/Resources/Private/Partials/UserBar.html
    dataProcessing {
        10 = Martinpfister\Base\DataProcessing\UserNameProcessor
    }
    settings {
        pageUids < site.pageUids
    }
}

lib.currentPageId = TEXT
lib.currentPageId.data = TSFE:id