# **********************************************************
# Mainly used in FLUID templates
# <f:cObject typoscriptObjectPath="lib.loadContent" data="0" />
# **********************************************************
lib.loadContent = COA
lib.loadContent {
    5 = LOAD_REGISTER
    5 {
        colPos.cObject = TEXT
        colPos.cObject {
            value.current = 1
            ifEmpty = 0
        }
    }
    20 < styles.content.get
    20.select.where = colPos={register:colPos}
    20.select.where.insertData = 1
}
