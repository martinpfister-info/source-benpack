# **********************************************************
# Slider tt_content, wrap as article.
# <f:cObject typoscriptObjectPath="lib.loadSlider" data="0" />
# Adds first, middle and last Classes too Wrap.
# **********************************************************

lib.loadSlider < lib.loadContent

lib.loadSlider {
    20.renderObj.stdWrap.wrap = |###SPLITTER###

    20.stdWrap.split {
        token = ###SPLITTER###
        cObjNum = 1 |*| 2 |*| 3 || 4

        1.current = 1
        1.wrap =  <article class="item">|</article>

        2.current = 1
        2.wrap =  <article class="item">|</article>

        3.current = 1
        3.wrap =  <article class="item active">|</article>

        4.current = 1
    }
}