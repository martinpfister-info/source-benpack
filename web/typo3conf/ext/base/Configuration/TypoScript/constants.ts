# **********************************************************
#    Extension constants
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:base/Resources/Private/Extensions/fluid_styled_content/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:products/Configuration/TypoScript/constants.ts">

# **********************************************************
#    EXT:base plugin constants
# **********************************************************
plugin.tx_base {
    view {
        layoutRootPath = EXT:base/Resources/Private/Layouts
        partialRootPath = EXT:base/Resources/Private/Partials
        templateRootPath = EXT:base/Resources/Private/Templates
    }
}

# **********************************************************
#    Company constants
# **********************************************************
company {
    name = BenPack
    street = Glütschbachstrasse 2
    pobox =
    city = 3661 Uetendorf
    phone = T +41 31 318 01 60
    fax = F +41 31 318 01 62
    website = www.benpack.ch
    email = info@benpack.ch
}

# **********************************************************
#    Site settings
# **********************************************************
site {
    # Page title
    pageTitlePrefix < company.name
    pageTitlePrefix := appendString( -)
    pageTitleSuffix =

    # Languages
    languageUids = 0,1,2
    languageLabels = Deutsch |*| Français |*| Italiano

    # Compress / merge CSS / JS
    compressAndMergeAssets = 0

    # realurl
    enableRealURL             = 1
    enableSocialMediaMetaTags = 0

    # Google analytics
    googleAnalytics = 1
    googleAnalytics.account = UA-112610328-1

    # Pids that may be used in various places
    pageUids {
        home = 1
        contact = 6
    }
    mailSetup {
        from {
            address = info@benpack.ch
        }
        replyTo {
            address = info@benpack.ch
        }
    }
}

# **********************************************************
#    change baseUrl according to http host
# **********************************************************
[globalString = ENV:HTTP_HOST=*benpack.local]
    config.baseURL = http://benpack.local/
[global]

[globalString = ENV:HTTP_HOST=benpack.ausruf.ch]
    config.baseURL = https://benpack.ausruf.ch
[global]

[globalString = ENV:HTTP_HOST=*benpack.ch]
    config.baseURL = https://www.benpack.ch
[global]
[globalString = ENV:HTTP_HOST=*benpack.de]
    config.baseURL = https://www.benpack.de
[global]
[globalString = ENV:HTTP_HOST=*benpack.at]
    config.baseURL = https://www.benpack.at
[global]


# **********************************************************
#    Content constants
# **********************************************************
content.img.srcset = 480,768,992,1200
