# **********************************************************
# Core Extensions
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:extbase/ext_typoscript_setup.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid/ext_typoscript_setup.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:products/Configuration/TypoScript/setup.ts">

# **********************************************************
# Base
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:./variables.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/config_language.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/config.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/page_title.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/page.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/page_socialmediametatags.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/page_felayoutrendering.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/page_jscssincludes.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/page_icons.ts">

# **********************************************************
# Extensions setups overwrites
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:base/Resources/Private/Extensions/fluid_styled_content/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:base/Resources/Private/Extensions/fluid_styled_responsive_images/setup.ts">

# **********************************************************
# Load typoscript template objects
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/temp/menu-language.ts">

# **********************************************************
# Load objects to be rendered
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/countContent.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/loadContent.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/loadSlider.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/skiplinks.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/menu.ts">
