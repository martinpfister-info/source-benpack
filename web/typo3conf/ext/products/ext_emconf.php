<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'Benpack Product Extension',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Martin Pfister',
	'author_email' => 'mail@martinpfister.info',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.0.0',
	'constraints' => [
		'depends' => [
			'typo3' => '7.6.0-8.9.99',
        ],
    ],
];
