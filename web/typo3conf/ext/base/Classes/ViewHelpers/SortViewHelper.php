<?php
namespace Martinpfister\Base\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 sozialinfo.ch
 *  All rights reserved
 *
 ***************************************************************/
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use Sozialinfo\SiJobs\Domain\Model\Search;

/**
 * SortViewHelper
 *
 * @author Martin Pfister <martin.pfister@sozialinfo.ch>, Sozialinfo.ch,
 *         Patryk Ostrowski <kontakt@amtsolution.pl>, AMT Solution
 */
class SortViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'a';

    /**
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerUniversalTagAttributes();
        $this->registerArgument('search', 'Object', 'Current SearchObject', true);
        $this->registerArgument('sorting', 'string', 'new sorting Field', true);
    }

    /**
     * @todo create uri with all searchAttributes
     * @return string
     */
    public function render()
    {
        /** @var \Sozialinfo\SiJobs\Domain\Model\Search $search */
        $search     = $this->arguments['search'];
        $sorting    = $this->arguments['sorting'];
        $linkTitle  = trim($this->renderChildren());
        $uriBuilder = $this->controllerContext->getUriBuilder();

        //create URI Arguments
        $arguments = $this->findArguments($sorting, $search);

        //create URI
        $uri = $uriBuilder->reset()->uriFor(null, $arguments, null, null, null);

        $this->tag->addAttribute('href', $uri);
        $this->tag->setContent($this->addUpAndDownArrows($linkTitle, $sorting, $search));
        $this->tag->forceClosingTag(true);

        return $this->tag->render();
    }

    /**
     * @param string $title
     * @param string $sorting
     * @param Object $search
     * @return string
     */
    protected function addUpAndDownArrows(string $title, string $sorting, $search)
    {
        if ($sorting !== $search->getSorting()) {
            return $title;
        }

        if ($search->getOrdering() === QueryInterface::ORDER_ASCENDING) {
            return $title . '<span class="si-icon-angle-down"></span>';
        }

        return $title . '<span class="si-icon-angle-up"></span>';
    }

    /**
     * creates the new search object
     * @todo maybe create a toArray method in Search and iterate
     * @param string $sorting
     * @param Object $search
     * @return string
     */
    protected function findArguments(string $sorting, $search)
    {
        $arguments = [
            'search' => [
                'sorting'    => $sorting,
                'ordering'   => $this->determineOrderings($sorting, $search),
            ]
        ];

        return $arguments;
    }

    /**
     * reverses ordering, if its the current sorting
     * @param string $sorting
     * @param Object $search
     * @return string
     */
    protected function determineOrderings(string $sorting, $search)
    {
        if ($sorting !== $search->getSorting()) {
            return QueryInterface::ORDER_ASCENDING;
        }

        if ($search->getOrdering() === QueryInterface::ORDER_ASCENDING) {
            return QueryInterface::ORDER_DESCENDING;
        }

        return QueryInterface::ORDER_ASCENDING;
    }
}
