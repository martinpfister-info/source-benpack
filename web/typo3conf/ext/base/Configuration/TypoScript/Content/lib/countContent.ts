# **********************************************************
# used in Slider PageTemplate
# <f:cObject typoscriptObjectPath="lib.countContent" data="0" />
# **********************************************************
lib.countContent = COA
lib.countContent {

    5 = LOAD_REGISTER
    5 {

        colPos.cObject = TEXT
        colPos.cObject {
            value.current = 1
            ifEmpty = 0
        }
    }

    20 = CONTENT
    20 {
        table = tt_content
        select {
            where = colPos=4
            where.insertData = {register:colPos}
            selectFields = COUNT(*) AS counter
        }

        renderObj     = TEXT
        renderObj.field = counter
        renderObj.wrap  = |
    }

}