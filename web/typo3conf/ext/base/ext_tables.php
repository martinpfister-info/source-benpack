<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$ll = 'LLL:EXT:'.$_EXTKEY.'/Resources/Private/Language/locallang_mod.xlf:';
$flex = 'EXT:' . $_EXTKEY . '/Configuration/Flexforms/';

/******************************
 * Backend skinning
 *****************************/
// Read extension 'backend' configuration
$backendExtConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend']);
// Set login logo, if it has not previously been set
if (empty($backendExtConf['loginLogo'])) {
    $backendExtConf['loginLogo'] = 'EXT:' . $_EXTKEY . '/Resources/Public/Backend/Skin/img/logo_login.png';
}
// Set login background image, if it has not previously been set
if (empty($backendExtConf['loginBackgroundImage'])) {
    $backendExtConf['loginBackgroundImage'] = 'EXT:' . $_EXTKEY . '/Resources/Public/Backend/Skin/img/bg_login.jpg';
}
// Re-serialize 'backend' extension configuration
$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'] = serialize($backendExtConf);