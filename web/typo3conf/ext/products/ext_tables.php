<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Pfister.' . $_EXTKEY,
    'Products',
    'Produkte'
);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    'Produkte'
);

// Register Model Table Translation
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
    'tx_products_domain_model_product',
    'EXT:products/Resources/Private/Language/locallang_csh_tx_products_domain_model_product.xlf'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
    'tx_products_domain_model_product'
);
