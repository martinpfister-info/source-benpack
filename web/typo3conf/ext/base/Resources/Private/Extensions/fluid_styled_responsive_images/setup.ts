# reset default css
#plugin.tx_ws_textmedia_bootstrap._CSS_DEFAULT_STYLE >

# overwrite Responsive Image srcset creator settings
tt_content.textmedia.settings.responsive_image_rendering {
    layoutKey = srcset
    sourceCollection >
    sourceCollection {
        10 {
            width = 1260m
            srcset = 1260w
        }
        20 {
            width = 992m
            srcset = 992w
        }

        30 {
            width = 768m
            srcset = 768w
        }

        40 {
            width = 480m
            srcset = 480w
        }

        50 {
            width = 360m
            srcset = 360w
        }
        60 >
    }
}
