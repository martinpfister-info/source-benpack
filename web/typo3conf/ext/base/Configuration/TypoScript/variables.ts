# **********************************************************
# Variables passed to TypoScript and PHP Code
# see page_felayoutrendering.ts to find Variables passed to Fluid
# **********************************************************
site {
    pageUids {
        home = {$site.pageUids.home}
    }
    mailSetup {
        from {
            address = {$site.mailSetup.from.address}
        }
        replyTo {
            address = {$site.mailSetup.replyTo.address}
        }
        admin {
            address = {$site.mailSetup.admin.address}
        }
    }
}
