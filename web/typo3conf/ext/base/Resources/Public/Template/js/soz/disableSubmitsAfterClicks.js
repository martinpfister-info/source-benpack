//root namespace
var soz = soz || {};

// prevents the users from clicking multiple times on form buttons.
//
// example: create JobOffer. if you click multiple times on "create JobOffer" (before the browser got the answer from the server,
// and the new page is loading) you would create more then one actual JobOffer.
soz.disableSubmitsAfterClicks = {
    init: function () {
        $('input[type=submit], button').click(function () {
            var button = $(this);
                //Timeout added because of problems with the Login Form (rsa)
                setTimeout(function () {
                    button.prop('disabled', true);
                }, 20);
                setTimeout(function () {
                    button.prop('disabled', false);
                }, 1500)

        })
    }
};
