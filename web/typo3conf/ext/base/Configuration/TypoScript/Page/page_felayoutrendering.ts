page.10 = FLUIDTEMPLATE
page.10 {
    # template paths
    templateRootPaths {
        10 = EXT:base/Resources/Private/Templates/Page
    }
    partialRootPaths {
        10 = EXT:base/Resources/Private/Partials
    }
    layoutRootPaths {
        10 = EXT:base/Resources/Private/Layouts
    }

    # get template file name dynamically
    templateName = TEXT
    templateName.stdWrap {
        cObject = TEXT
        cObject {
            data = pagelayout
            # split selected layout ([extkey]__BackendLayoutName) by '__' and return last part as layout name
            split {
                token = __
                returnKey = 1
            }
        }
        ifEmpty = SingleColumn
    }

    # passing values to FLUIDTEMPLATE for later use
    settings {
        company.name = {$company.name}
        company.street = {$company.street}
        company.pobox = {$company.pobox}
        company.city = {$company.city}
        company.phone = {$company.phone}
        company.fax = {$company.fax}
        company.website = {$company.website}
        company.email = {$company.email}
        site {
            pageUids {
                home = {$site.pageUids.home}
            }
        }
    }

    variables {
        pagetitle = TEXT
        pagetitle.data = page:title
    }
}
