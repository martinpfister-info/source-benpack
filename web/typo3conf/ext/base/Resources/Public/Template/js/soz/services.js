//root namespace
var soz = soz || {};

/*
    services teaser module
    makes the height of the containers the same
 */
soz.services = {
    config: {
        advantages : '.service-advantage .advantage-list',
        info : '.service-info > .membership-info',
        maxHeight : 0
    },
    init: function() {
        if ($(this.config.advantages).length) {
            var elements = $(this.config.advantages);
            var maxHeight = this.config.maxHeight;
            this.normalizeHeight(elements, maxHeight);
            var infoBox = $(this.config.info);
            this.normalizeHeight(infoBox, maxHeight);
        }
    },
    normalizeHeight: function (elements, maxHeight) {
        elements.each(function () {
            maxHeight = ($(this).height() > maxHeight ? $(this).height() : maxHeight);
        });
        elements.height(maxHeight);
    }
};
