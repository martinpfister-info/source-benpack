# **********************************************************
# Adds Favicon for the Browser and one png-Icon for Mobile/Tablets.
# **********************************************************
page.headerData.20 = COA
page.headerData.20 {

    500 = TEXT
    500 {
        prepend = TEXT
        prepend.char = 10
        stdWrap.data = PATH:EXT:base/Resources/Public/Template/images/favicon.ico
        wrap = <link href="|" rel="shortcut icon" type="image/x-icon">
        required = 1
        stdWrap.required = 1
    }

    #600 = TEXT
    #600 {
    #    prepend = TEXT
    #    prepend.char = 10
    #    stdWrap.data = PATH:EXT:base/Resources/Public/Template/images/logo_klein.gif
    #    wrap = <link href="|" rel="icon" sizes="40x43" type="image/png">
    #    required = 1
    #    stdWrap.required = 1
    #}
}
