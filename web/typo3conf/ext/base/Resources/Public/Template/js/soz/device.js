//root namespace
var soz = soz || {};

//device modul
soz.device = {
    init : function(){
        if ($(window).width() <= 767) {
            return 'xsmall';
        } else if ($(window).width() > 767 && $(window).width() <=991) {
            return 'small';
        } else if($(window).width() > 991 && $(window).width() <=1199){
            return 'medium';
        } else{
            return 'standard';
        }
    }
};
