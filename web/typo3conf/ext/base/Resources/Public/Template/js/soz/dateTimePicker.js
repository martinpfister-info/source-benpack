//root namespace
var soz = soz || {};

/* the datepicker function is from the following plugin:
 * https://bootstrap-datepicker.readthedocs.io/en/latest/markup.html
 *
 * <div class="input-group date">
 *    <input type="text" class="form-control" value="12-02-2012">
 *    <div class="input-group-addon">
 *      <span class="glyphicon glyphicon-th"></span>
 *    </div>
 *  </div>
 */
soz.dateTimePicker = {
    config: {
        elements: $(".date"),
        minDate: $("#minDate"),
        maxDate: $("#maxDate")
    },
    init: function () {
        soz.dateTimePicker.beforeTomorrow(this.config.minDate);
        soz.dateTimePicker.beforeTomorrow(this.config.maxDate);
        soz.dateTimePicker.keepCorrectDate();
        soz.dateTimePicker.laterThanYesterday();
    },
    /**
     * set datapicker with date today or later.
     */
    laterThanYesterday: function () {
        if (this.config.elements.length > 0) {
            $(this.config.elements).datepicker({
                language: 'de',
                format: 'dd.mm.yyyy',
                autoclose: true,
                todayHighlight: true,
                startDate: new Date()
            });
        }
    },
    /**
     * Datapicker with limit to today day.
     */
    beforeTomorrow: function (datapickerElementID) {
        if (this.config.minDate.length > 0) {
            $(datapickerElementID).datepicker({
                language: 'de',
                format: 'dd.mm.yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: new Date()
            });
        }
    },
    /**
     * if we set min and max date, this method give us ability to keep this date correct.
     */
    keepCorrectDate: function () {
        var minDate = soz.dateTimePicker.config.minDate.data('datepicker');
        var maxDate = soz.dateTimePicker.config.maxDate.data('datepicker');
        this.config.minDate.change(function () {
            if (minDate.getDate() && maxDate.getDate()) {
                if (minDate.getDate() > maxDate.getDate()) {
                    maxDate.setDate(minDate.getDate());
                }
            }
        });
        this.config.maxDate.change(function () {
            if (minDate.getDate() && maxDate.getDate()) {
                if (minDate.getDate() > maxDate.getDate()) {
                    minDate.setDate(maxDate.getDate());
                }
            }
        });
    }
};
