
plugin.tx_products_products {
	view {
		templateRootPaths.0 = {$plugin.tx_products_products.view.templateRootPath}
		partialRootPaths.0 = {$plugin.tx_products_products.view.partialRootPath}
		layoutRootPaths.0 = {$plugin.tx_products_products.view.layoutRootPath}
	}
	persistence {
		storagePid = 8
	}
}