//root namespace
var soz = soz || {};

//menu modul
soz.badge = {

    config: {
        active: '.aside-menu nav .active',
        activeText: '.aside-menu nav .active > a',
        badge: '#shortcut-bar a',
        activeBadge: '#shortcut-bar .soz-circle-s',
    },

    init: function() {
        if ($(this.config.active).length) {
            var text = $(this.config.activeText).text();
            $(this.config.badge).each(function(){
                if($(this).text().indexOf(text) != -1){
                    $(this).find('.soz-circle-s').addClass('soz-current-icon')
                }
            });
        }
    }
};
