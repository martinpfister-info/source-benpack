<?php
namespace Martinpfister\Base\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 sozialinfo.ch
 *  All rights reserved
 ***************************************************************/

/**
 * This interface contains common status definitions
 *
 * @author Dmitry Dulepov <support@snowflake.ch>
 * @author Krzysztof Napora <k.napora@amtsolution.pl>
 */
interface StatusDefinition
{
    const STATUS_DRAFT = 10;
    const STATUS_READY = 20;
    const STATUS_PUBLISHED = 30;
    const STATUS_ARCHIVED = 40;
}
