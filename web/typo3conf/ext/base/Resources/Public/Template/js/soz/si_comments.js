var soz = soz || {};

soz.comments = {
	init: function() {
		var form = $('.tx-sicomments-form'),
				closeLink = form.find('.comment-in-reply-to .form-control .close'),
				commentIdElement = form.find('input[name="tx_sicomments_commentform[comment][parentComment]"]');

		closeLink.on('click', function () {
			form.find('.comment-in-reply-to').addClass('hidden');
			commentIdElement.val('');
		});

		$('.comment-reply-link').on('click', function () {
			var link = $(this),
					form = $('.tx-sicomments-form'),
					commentId = link.data('comment-id'),
					commentIdElement = form.find('input[name="tx_sicomments_commentform[comment][parentComment]"]'),
					inReplyToElement = form.find('.comment-in-reply-to .form-control .quoted-comment');

			// Set hidden reply id
			commentIdElement.val(commentId);
			inReplyToElement.html($('.comment-' + commentId + ' .comment-content').html());
			form.find('.comment-in-reply-to').removeClass('hidden');

			// Scroll to form
			$('html, body').animate({
				scrollTop: form.offset().top
			}, 500);
		});
	}
};
