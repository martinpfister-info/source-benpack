//root namespace
var soz = soz || {};

//menu modul
soz.sifemanager = {

    config: {
        active: '.active',
        placeholder: '.label-placeholder',
        field: '.label-placeholder input'
    },

    init: function() {
        $(this.config.field).each(function(){

            // Check input values on postback and add class "active" if value exists
            if ($(this).val() !== '') {
                $(this).parent().addClass("active");
            }

            // on focus add class "active" to label
            $(this).focus(function(){
                $(this).parent().addClass("active");
            });

            // on blur check field and remove class if needed
            $(this).blur(function(){
                if($(this).val() === '' ) {
                    $(this).parent().removeClass('active');
                }
            });
        });
    }
};
