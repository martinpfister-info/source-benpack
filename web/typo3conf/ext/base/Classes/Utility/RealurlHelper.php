<?php
namespace Martinpfister\Base\Utility;

/***************************************************************
*  Copyright notice
*
*  (c) 2017 sozialinfo.ch
*  All rights reserved
***************************************************************/

/**
 * This class contains a heloer for realurl to convert controller names to
 * lowercase. This makes urls better looking.
 *
 * @author Dmitry Dulepov <support@snowflake.ch>
 */
class RealurlHelper
{
    /**
     * Converts controller name to lower or upper case as needed.
     *
     * @param array $parameters
     * @return string
     */
    public function convertControllerName(array $parameters)
    {
        if ($parameters['decodeAlias']) {
            $result = ucfirst($parameters['value']);
        } else {
            $result = lcfirst($parameters['value']);
        }

        return $result;
    }
}
