//root namespace
var soz = soz || {};

//menu modul
soz.menu = {

    config: {
        toggleLink: '#menu-toggle > a',
        toggleIcon: '#menu-cross-icon',
        topBar: '#top-bar',
        mainMenu: '#si-menu-mobile',
        login: '#si-hairu-mobile',
        opened: '#si-menu-mobile .mm-opened'
    },

    binding: function() {
        var that = this;
        var $login = $(that.config.login).attr('id', null).removeClass('hidden').detach();

        $(that.config.mainMenu).mmenu({
            "offCanvas": {
                "zposition": "top",
                "position": "bottom"
            },
            "setSelected": {
                "current": "detect"
            },
            "extensions": [
                "theme-black",
                "fullscreen",
                "multiline",
                "listview-large",
                "border-full"
            ]
        });

        var API = $(that.config.mainMenu).data('mmenu');

        $(that.config.toggleLink).on( "click", function(event) {
            event.preventDefault();
            API.open();
        });

        API.bind( "open:finish", function() {
            //Append login on open panel on openMenu
            $(that.config.opened).append($login);
            //Change menu icon
            setTimeout(function() {
                $(that.config.toggleIcon).toggleClass('open');
            }, 100);
        });
        API.bind( "close:finish", function() {
            //Change menu icon
            setTimeout(function() {
                $(that.config.toggleIcon).toggleClass('open');
            }, 100);
        });
        API.bind( "openPanel:start", function( $panel ) {
            //Append login on open panel
            $panel.append($login);
        });

    },


    init: function() {
        this.binding();
    }

};
