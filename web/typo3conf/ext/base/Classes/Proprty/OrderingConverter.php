<?php
namespace Martinpfister\Base\Property\TypeConverter;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 sozialinfo.ch
 *  All rights reserved
 ***************************************************************/
use TYPO3\CMS\Extbase\Property\TypeConverter\AbstractTypeConverter;

/**
 * Class OrderingConverter
 *
 * @author Martin Pfister <martin.pfister@sozialinfo.ch>, Sozialinfo.ch
 * @author Michal Cygnakiewicz <mc@amtsolution.pl">, amtsolution.pl
 */
class OrderingConverter extends AbstractTypeConverter
{
    /**
     * @var array<string>
     */
    protected $sourceTypes = ['string'];

    /**
     * @var string
     */
    protected $targetType = \TYPO3\CMS\Extbase\Persistence\QueryInterface::class;

    /**
     * @var int
     */
    protected $priority = 1;

    /**
     * Actually convert from $source to $targetType, taking into account the fully
     * built $convertedChildProperties and $configuration.
     *
     * The return value can be one of three types:
     * - an arbitrary object, or a simple type (which has been created while mapping).
     *   This is the normal case.
     * - NULL, indicating that this object should *not* be mapped (i.e. a "File Upload" Converter could return NULL if no file has been uploaded, and a silent failure should occur.
     * - An instance of \TYPO3\CMS\Extbase\Error\Error -- This will be a user-visible error message later on.
     * Furthermore, it should throw an Exception if an unexpected failure (like a security error) occurred or a configuration issue happened.
     *
     * @param mixed $source
     * @param string $targetType
     * @param array $convertedChildProperties
     * @param \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration
     * @return mixed|\TYPO3\CMS\Extbase\Error\Error the target type, or an error object if a user-error occurred
     * @throws \TYPO3\CMS\Extbase\Property\Exception\TypeConverterException thrown in case a developer error occurred
     * @api
     */
    public function convertFrom($source, $targetType, array $convertedChildProperties = [], \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration = null)
    {
        if ($source === 'ASC') {
            return \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;
        }

        return \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING;
    }
}
