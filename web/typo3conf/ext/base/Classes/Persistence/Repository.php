<?php
namespace Martinpfister\Base\Persistence;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 sozialinfo.ch
 *  All rights reserved
 ***************************************************************/

use TYPO3\CMS\Extbase\Persistence\Repository as CoreRepository;

/**
 * Repository
 * Extend repository to setRespectStoragePage to false
 *
 * @author Patryk Ostrowski <kontakt@amtsolution.pl>, amtsolution.pl
 */
class Repository extends CoreRepository
{
    /**
     * Disables storage pid check: we want to have records from any pid!
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     */
    public function createQuery()
    {
        $query = parent::createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);

        return $query;
    }
}
