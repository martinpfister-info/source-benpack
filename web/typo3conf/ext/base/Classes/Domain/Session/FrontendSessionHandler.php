<?php
namespace Martinpfister\Base\Domain\Session;

class FrontendSessionHandler
{

    /**
     * Keeps session mode.
     * Either 'ses' or 'user'.
     *
     * @var string
     */
    protected $mode = 'ses';

    /**
     * The User-Object with the session-methods.
     *
     * @var object
     */
    protected $sessionObject = null;

    /**
     * The key the data is stored in the session.
     * @var string
     */
    protected $storageKey = 'sozialinfo';

    /**
     * Class constructor.
     * @param string $mode
     * @param string $storageKey
     * @throws \Exception
     */
    public function __construct($mode, $storageKey)
    {
        if ($mode) {
            $this->mode = $mode;
        }

        if ($this->mode === null || ($this->mode != "ses" && $this->mode != "user")) {
            throw new \Exception("Session-Mode is not defined!", 1388660107);
        }

        if ($storageKey) {
            $this->storageKey = $storageKey;
        }

        $this->sessionObject = $GLOBALS['TSFE']->fe_user;
    }

    /**
     * Store value in session
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function store($key, $value)
    {
        $sessionData = $this->sessionObject->getKey($this->mode, $this->storageKey);
        $sessionData[$key] = $value;
        $this->sessionObject->setKey($this->mode, $this->storageKey, $sessionData);
        $this->sessionObject->storeSessionData();
    }

    /**
     * Delete value in session
     * @param string $key
     * @return void
     */
    public function delete($key)
    {
        $sessionData = $this->sessionObject->getKey($this->mode, $this->storageKey);
        unset($sessionData[$key]);
        $this->sessionObject->setKey($this->mode, $this->storageKey, $sessionData);
        $this->sessionObject->storeSessionData();
    }

    /**
     * Read value from session
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        $sessionData = $this->sessionObject->getKey($this->mode, $this->storageKey);
        return isset($sessionData[$key]) ? $sessionData[$key] : null;
    }
}
