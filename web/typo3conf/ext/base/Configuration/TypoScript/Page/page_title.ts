# Page <title> tag
config {
    # Title
    pageTitle.cObject = COA
    pageTitle.cObject {
        10 = TEXT
        10.value = {$site.pageTitlePrefix}
        # Insert space character between prefix and page title, if prefix is set
        10.stdWrap.noTrimWrap = || |
        10.stdWrap.if.isTrue = {$site.pageTitlePrefix}

        20 = TEXT
        20.field = subtitle // title
        20.override.cObject = TEXT
        20.override.cObject.data = TSFE:altPageTitle

        30 = TEXT
        30.value = {$site.pageTitleSuffix}
        # Insert space character between page title and suffix, if suffix is set
        30.stdWrap.noTrimWrap = | ||
        30.stdWrap.if.isTrue = {$site.pageTitleSuffix}
    }
}

# page <h1>
# on start page use different colors
# on level 1,2 combine the two levels
# only for level 3+ or extension show page use the record
lib.rootlinePageTitle = COA
lib.rootlinePageTitle {
    wrap = <h1>|</h1>
    10 = HMENU
    10.special = rootline
    10.special.range = 1|1
    10.1 = TMENU
    10.1.wrap = <span class="level-1">|</span>
    10.1.NO.doNotLinkIt = 1
    # noTrimWrap adds a space after the span tag, make it possible to have the subtitle starting on the same line.
    10.1.NO.stdWrap.cObject = COA
    10.1.NO.stdWrap.cObject {
        10 = TEXT
        10.field = title
        10.noTrimWrap = |<span class="title"> | </span>| |
        20 = TEXT
        20.field = subtitle
        20.noTrimWrap = |<span class="subtitle"> | </span>| |
    }
    20 < .10
    20.special.range = 2|1
    20.1.wrap = <span class="level-2">|</span>
}

[treeLevel = 0]
    # homepage
    lib.rootlinePageTitle.10.special.range = 0|1
[treeLevel = 2,3,4,5]
    # remove subtitles on level 2
    lib.rootlinePageTitle.10.1.NO.stdWrap.cObject.20 >
    lib.rootlinePageTitle.20.1.NO.stdWrap.cObject.20 >
[global]

### display as page title job offer title from db
[globalVar = GP:tx_sijobs_joboffers|jobOffer > 0]
    # create new og tags based on extension data
    page.meta.og:title >
    page.meta.og:description >
    page.meta.og:image >
    page.meta.og:url >
    config.pageTitle.cObject.40 = RECORDS
    config.pageTitle.cObject.40 {
        dontCheckPid = 1
        tables = tx_sijobs_domain_model_joboffer
        source = {GP:tx_sijobs_joboffers|jobOffer}
        source.insertData = 1
        conf.tx_sijobs_domain_model_joboffer >
        conf.tx_sijobs_domain_model_joboffer = TEXT
        conf.tx_sijobs_domain_model_joboffer {
            field = title
        }
        stdWrap.noTrimWrap = | - ||
    }
[global]

### display as page title job offer title from db
[globalVar = GP:tx_sieducation_educationlists|education > 0]
    # create new og tags based on extension data
    page.meta.og:title >
    page.meta.og:description >
    page.meta.og:image >
    page.meta.og:url >
    config.pageTitle.cObject.40 = RECORDS
    config.pageTitle.cObject.40 {
        dontCheckPid = 1
        tables = tx_sieducation_domain_model_education
        source = {GP:tx_sieducation_educationlists|education}
        source.insertData = 1
        conf.tx_sieducation_domain_model_education >
        conf.tx_sieducation_domain_model_education = TEXT
        conf.tx_sieducation_domain_model_education {
            field = title
    }
    stdWrap.noTrimWrap = | - ||
    }
[global]

[PIDinRootline = {$site.pageUids.userAccount.start}]
lib.rootlinePageTitle = COA
lib.rootlinePageTitle {
    wrap = <h1 class="headerLine"><span class="level-1">|</span></h1>
    10 = COA
    10 {
        20 = TEXT
        20 {
            data = DB:pages:{$site.pageUids.userAccount.start}:title
            wrap = <span class="title"> | </span><br/>
        }
        30 = COA
        30 {
            5 = TEXT
            5 {
                data = TSFE:fe_user|user|first_name
                wrap = |&nbsp;
            }
            10 = TEXT
            10 {
                data = TSFE:fe_user|user|last_name
            }
            wrap = <span class="subtitle"> | </span>
        }

    }
}
[end]

[PIDinRootline = {$site.pageUids.institutionAccount.overview}]
lib.rootlinePageTitle = COA
lib.rootlinePageTitle {
    wrap = <h1 class="headerLine"><span class="level-1">|</span></h1>
    10 = COA
    10 {
        20 = TEXT
        20 {
            data = DB:pages:{$site.pageUids.institutionAccount.overview}:title
            wrap = <span class="title">|</span><br />
        }
        31 = TEXT
        31.value = <span class="subtitle">
        32 = USER_INT
        32 {
            userFunc = Sozialinfo\SiFemanager\Utility\UserUtility->getPreferredInstitutionTitle
        }
        33 = TEXT
        33.value = </span>
    }
}
[end]


[PIDinRootline = {$site.pageUids.login}]
lib.rootlinePageTitle = COA
lib.rootlinePageTitle {
    wrap = <h1 class="headerLine"><span class="level-1">|</span></h1>
    10 = COA
    10 {
        20 = TEXT
        20 {
            data = DB:pages:{$site.pageUids.login}:title
            wrap = <span class="title">|</span><br />
        }
        31 = TEXT
        31.value = <span class="subtitle">
        33 = TEXT
        33.value = </span>
    }
}
[end]

[PIDinRootline = {$site.pageUids.register}]
lib.rootlinePageTitle = COA
lib.rootlinePageTitle {
    wrap = <h1 class="headerLine"><span class="level-1">|</span></h1>
    10 = COA
    10 {
        20 = TEXT
        20 {
            data = DB:pages:{$site.pageUids.register}:title
            wrap = <span class="title">|</span><br />
        }
        31 = TEXT
        31.value = <span class="subtitle">
        33 = TEXT
        33.value = </span>
    }
}
[end]