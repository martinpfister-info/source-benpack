<?php
namespace Martinpfister\Base\Utility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin Pfister <martin.pfister@martinpfister.info>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Utility;

class ExtbaseHelper implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * Debugs a SQL query from a QueryResult
     *
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryResult
     * @param boolean $explainOutput
     * @param boolean $printToFrontend
     * @return void
     */
    static function debugQuery($queryResult, $explainOutput = false, $printToFrontend = true)
    {
        if ($queryResult == null) {
            return null;
        }

        $GLOBALS['TYPO3_DB']->debugOuput = 2;
        $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;
        if ($explainOutput) {
            $GLOBALS['TYPO3_DB']->explainOutput = true;
        }

        // Provoke Query
        $queryResult->toArray();
        $query = $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;

        // Set Breakpoint on next Line
        if ($printToFrontend) {
            Utility\DebuggerUtility::var_dump(
                $query, 'debug_lastBuiltQuery'
            );
        }

        //Restore Settings
        $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = false;
        $GLOBALS['TYPO3_DB']->explainOutput = false;
        $GLOBALS['TYPO3_DB']->debugOuput = false;
    }

    /**
     * @param object $object
     */
    public static function validateObject($object)
    {
        $validatorResolver = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Validation\\ValidatorResolver');
        /* @var $validatorResolver \TYPO3\CMS\Extbase\Validation\ValidatorResolver */
        $validator = $validatorResolver->getBaseValidatorConjunction(get_class($object));
        /* @var $validator \TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator */
        $result = $validator->validate($object);

        if(!$result->getFlattenedErrors()){
            return true;
        }
        return false;
    }

    /**
     * Sort given QueryResults by uids of records
     * Not possible by core at current state
     * https://forge.typo3.org/issues/33345
     *
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryResult
     * @param array $uids
     * @return array sorted results
     */
    public static function reorderQueryByUids($queryResult, $uids)
    {
        $sortedResults = array();
        foreach ($uids as $uid) {
            foreach ($queryResult as $result) {
                if ($uid == $result->getUid()) {
                    $sortedResults[] = $result;
                    continue;
                }
            }
        }

        return $sortedResults;
    }
}
