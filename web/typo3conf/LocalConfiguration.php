<?php
return [
    'BE' => [
        'debug' => '',
        'explicitADmode' => 'explicitAllow',
        'installToolPassword' => '$P$CxNdrm24ZhEiuZapc7wyzvTm44q2Gp.',
        'loginSecurityLevel' => 'rsa',
    ],
    'DB' => [
        'Connections' => [
            'Default' => [
                'charset' => 'utf8',
                'dbname' => 'sozistage',
                'driver' => 'mysqli',
                'host' => 'localhost',
                'password' => 'EHQqkjNwpYhefHwg2oVE',
                'user' => 'sozistage',
            ],
        ],
    ],
    'EXT' => [
        'extConf' => [
            'devlog' => 'a:10:{s:15:"minimumLogLevel";s:2:"-1";s:11:"excludeKeys";s:0:"";s:11:"includeKeys";s:0:"";s:8:"ipFilter";s:1:"*";s:16:"refreshFrequency";s:1:"4";s:14:"entriesPerPage";s:2:"25";s:11:"maximumRows";s:4:"1000";s:13:"optimizeTable";s:1:"1";s:20:"maximumExtraDataSize";s:7:"1000000";s:11:"logFilePath";s:0:"";}',
            'error404page' => 'a:5:{s:19:"doktypeError404page";s:3:"104";s:14:"enableErrorLog";s:1:"0";s:22:"excludeErrorLogPattern";s:0:"";s:19:"basicAuthentication";s:0:"";s:5:"debug";s:1:"0";}',
            'femanager' => 'a:2:{s:13:"disableModule";s:1:"0";s:10:"disableLog";s:1:"0";}',
            'fluid_styled_responsive_images' => 'a:1:{s:23:"enableSmallDefaultImage";s:1:"1";}',
            'hairu' => 'a:0:{}',
            'news' => 'a:20:{s:29:"removeListActionFromFlexforms";s:1:"2";s:20:"pageModuleFieldsNews";s:317:"LLL:EXT:news/Resources/Private/Language/locallang_be.xlf:pagemodule_simple=title,datetime;LLL:EXT:news/Resources/Private/Language/locallang_be.xlf:pagemodule_advanced=title,datetime,teaser,categories;LLL:EXT:news/Resources/Private/Language/locallang_be.xlf:pagemodule_complex=title,datetime,teaser,categories,archive;";s:24:"pageModuleFieldsCategory";s:17:"title,description";s:11:"archiveDate";s:4:"date";s:13:"prependAtCopy";s:1:"1";s:6:"tagPid";s:1:"1";s:25:"showMediaDescriptionField";s:1:"0";s:12:"rteForTeaser";s:1:"0";s:22:"contentElementRelation";s:1:"1";s:13:"manualSorting";s:1:"0";s:19:"categoryRestriction";s:0:"";s:34:"categoryBeGroupTceFormsRestriction";s:1:"0";s:6:"useFal";s:1:"1";s:19:"dateTimeNotRequired";s:1:"0";s:12:"showImporter";s:1:"0";s:18:"storageUidImporter";s:1:"1";s:22:"resourceFolderImporter";s:12:"/news_import";s:24:"showAdministrationModule";s:1:"1";s:21:"contentElementPreview";s:1:"1";s:35:"hidePageTreeForAdministrationModule";s:1:"0";}',
            'products' => 'a:0:{}',
            'realurl' => 'a:6:{s:10:"configFile";s:26:"typo3conf/realurl_conf.php";s:14:"enableAutoConf";s:1:"1";s:14:"autoConfFormat";s:1:"0";s:17:"segTitleFieldList";s:0:"";s:12:"enableDevLog";s:1:"0";s:10:"moduleIcon";s:1:"1";}',
            'recycler' => 'a:0:{}',
            'rsaauth' => 'a:1:{s:18:"temporaryDirectory";s:0:"";}',
            'rte_ckeditor' => 'a:0:{}',
            'rx_shariff' => 'a:5:{s:8:"services";s:85:"GooglePlus, Facebook, LinkedIn, Reddit, StumbleUpon, Flattr, Pinterest, Xing, AddThis";s:14:"allowedDomains";s:11:"SERVER_NAME";s:3:"ttl";s:4:"3600";s:15:"facebook_app_id";s:0:"";s:15:"facebook_secret";s:0:"";}',
            'saltedpasswords' => 'a:2:{s:3:"BE.";a:4:{s:21:"saltedPWHashingMethod";s:41:"TYPO3\\CMS\\Saltedpasswords\\Salt\\PhpassSalt";s:11:"forceSalted";i:0;s:15:"onlyAuthService";i:0;s:12:"updatePasswd";i:1;}s:3:"FE.";a:5:{s:7:"enabled";i:1;s:21:"saltedPWHashingMethod";s:41:"TYPO3\\CMS\\Saltedpasswords\\Salt\\PhpassSalt";s:11:"forceSalted";i:0;s:15:"onlyAuthService";i:0;s:12:"updatePasswd";i:1;}}',
            'scheduler' => 'a:4:{s:11:"maxLifetime";s:4:"1440";s:11:"enableBELog";s:1:"1";s:15:"showSampleTasks";s:1:"1";s:11:"useAtdaemon";s:1:"0";}',
            'sf_banners' => 'a:0:{}',
            'si_address' => 'a:0:{}',
            'si_article' => 'a:0:{}',
            'si_categories' => 'a:0:{}',
            'si_comments' => 'a:0:{}',
            'si_expertforum' => 'a:0:{}',
            'si_femanager' => 'a:0:{}',
            'si_importer' => 'a:0:{}',
            'si_institution' => 'a:0:{}',
            'si_jobs' => 'a:0:{}',
            'si_news' => 'a:0:{}',
            'si_notificationcenter' => 'a:0:{}',
            'si_team' => 'a:0:{}',
            'typo3_console' => 'a:0:{}',
            'vhs' => 'a:0:{}',
            'ws_textmedia_bootstrap' => 'a:1:{s:32:"loadContentElementWizardTsConfig";s:1:"0";}',
        ],
    ],
    'EXTCONF' => [
        'helhum-typo3-console' => [
            'initialUpgradeDone' => true,
        ],
        'lang' => [
            'availableLanguages' => [
                'de',
            ],
        ],
    ],
    'FE' => [
        'debug' => false,
        'loginSecurityLevel' => 'normal',
    ],
    'GFX' => [
        'jpg_quality' => '80',
        'processor' => 'ImageMagick',
        'processor_allowTemporaryMasksAsPng' => false,
        'processor_colorspace' => 'sRGB',
        'processor_effects' => 1,
        'processor_enabled' => true,
        'processor_path' => '/usr/bin/',
        'processor_path_lzw' => '/usr/bin/',
    ],
    'INSTALL' => [],
    'MAIL' => [
        'transport' => 'mail',
        'transport_sendmail_command' => '/usr/sbin/sendmail -t -i ',
        'transport_smtp_encrypt' => '',
        'transport_smtp_password' => '',
        'transport_smtp_server' => 'localhost:25',
        'transport_smtp_username' => '',
    ],
    'SYS' => [
        'caching' => [
            'cacheConfigurations' => [
                'extbase_object' => [
                    'frontend' => 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend',
                    'groups' => [
                        'system',
                    ],
                    'options' => [
                        'defaultLifetime' => 0,
                    ],
                ],
            ],
        ],
        'devIPmask' => '',
        'displayErrors' => '',
        'enableDeprecationLog' => '',
        'encryptionKey' => '47db04707103de88fcf550fa9a0fe5765823b529857bb9ec31bb0590e1f0e3ab654b2d7100de2915bfea779f98b8ad61',
        'exceptionalErrors' => 20480,
        'isInitialDatabaseImportDone' => true,
        'isInitialInstallationInProgress' => false,
        'sitename' => 'benpack.ch',
        'sqlDebug' => '0',
        'systemLog' => 'error_log',
        'systemLogLevel' => '2',
    ],
];
