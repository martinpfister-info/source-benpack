<?php
namespace Martinpfister\Base\Domain\Validator;

/***************************************************************
*  Copyright notice
*
*  (c) 2017 sozialinfo.ch
*  All rights reserved
***************************************************************/

use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;

/**
 * This class contains a validator for "terms and conditions" checkbox.
 * If the field is false, it will add an error sayings that terms and
 * conditions must be accepted.
 *
 * @author Dmitry Dulepov <support@snowflake.ch>
 *
 * Credits also go to krzysztof <krzysztof.napora@yahoo.com>, who implemented
 * similar approach in EXT:si_jobs.
 */
class TermsAndConditionsValidator extends AbstractValidator
{
    /**
     * Allows this validator to accept empty values.
     *
     * @var bool
     */
    protected $acceptsEmptyValues = false;

    /**
     * Allows to specify custom error messages for the validation.
     *
     * @var array
     */
    protected $supportedOptions = [
        'key' => ['error.terms_and_conditions', 'Language key to use for the error message', 'string', false],
        'extension' => ['sozialinfo', 'Extension to fetch the label from', 'string', false],
    ];

    /**
     * Check if $value is valid. If it is not valid, adds an error
     * to result.
     *
     * @param mixed $value
     * @return void
     */
    protected function isValid($value)
    {
        if (empty($value)) {
            $this->addError(
                $this->translateErrorMessage(
                    $this->options['key'],
                    $this->options['extension']
                ),
                1262341470
            );
        }
    }
}
