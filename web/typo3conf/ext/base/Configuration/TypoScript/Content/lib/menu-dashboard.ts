lib.menu-feuser = HMENU
lib.menu-feuser.special = directory
lib.menu-feuser.special.value = {$site.pageUids.userAccount.overview}
lib.menu-feuser {
    1 = TMENU
    1 {
        wrap = <ul>|</ul>
        NO = 1
        NO {

            linkWrap = <li class="btn btn-primary btn-md">|</li>
            stdWrap.htmlSpecialChars = 1
        }
    }
}
