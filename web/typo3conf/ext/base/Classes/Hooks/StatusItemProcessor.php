<?php
namespace Martinpfister\Base\Hooks;

/***************************************************************
*  Copyright notice
*
*  (c) 2017 sozialinfo.ch>
*  All rights reserved
***************************************************************/

use Sozialinfo\SiJobs\Domain\Model\JobOffer;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * This class will prevent statuses other than "draft" showing for new job
 * offers and new applications. This is necessary because of price calculations:
 * price is calculated when the status is set to "published" but it can only be
 * properly calculated for already saved records.
 *
 * @author Dmitry Dulepov <support@snowflake.ch>
 */
class StatusItemProcessor
{

    /**
     * Adjusts status if necessary.
     *
     * @param array $parameters
     */
    public function checkRecordStatusItems(array &$parameters)
    {
        if (!isset($parameters['row']['uid']) || !MathUtility::canBeInterpretedAsInteger($parameters['row']['uid'])) {
            $items = [];
            foreach ($parameters['items'] as $item) {
                if ($item[1] == JobOffer::STATUS_DRAFT) {
                    $items[] = $item;
                    break;
                }
            }
            $parameters['items'] = $items;
        } else {
            // Make sure "published" and "archived" are unselectable because
            // we publish and archive only via scheduler job!
            $items = $parameters['items'];
            foreach ([ JobOffer::STATUS_PUBLISHED, JobOffer::STATUS_ARCHIVED ] as $statusToUnset) {
                if ((int)$parameters['row']['status'] !== $statusToUnset) {
                    $items = $this->unsetStatusOption($items, $statusToUnset);
                }
            }
            $parameters['items'] = $items;
        }
    }

    /**
     * Unsets status item that matches to the given status.
     *
     * @param array $items
     * @param int $statusToUnset
     * @return array
     */
    protected function unsetStatusOption(array $items, int $statusToUnset)
    {
        $result = [];

        foreach ($items as $item) {
            if ((int)$item[1] !== $statusToUnset) {
                $result[] = $item;
            }
        }

        return $result;
    }
}
