<?php
namespace Martinpfister\Base\ViewHelpers\Form;

/**
 * Original Code: https://github.com/svewap/extbase_fluid_form_validation
 */
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class HasErrorViewHelper extends AbstractViewHelper
{
    /**
     * @param string $for
     * @param string $then
     * @return string
     */
    public function render($for = '', $then = '')
    {
        $propertyResults   = null;
        $validationResults = $this->controllerContext->getRequest()->getOriginalRequestMappingResults();

        if ($validationResults !== null && $for !== '') {
            $propertyResults = $validationResults->forProperty($for);
        }

        if ($propertyResults->hasErrors()) {
            return $then;
        }

        return "";
    }
}
