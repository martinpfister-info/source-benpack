<?php
namespace Martinpfister\Base\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 sozialinfo.ch
 *  All rights reserved
 ***************************************************************/

/**
 * This class give as access to read typoscript setup in TCA
 *
 * @author Krzysztof Napora <k.napora@amtsolution.pl>
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TypoScript
{
    /**
     * Read TypoScript setting for plugin
     * @param  string $pluginName
     * @return array
     */
    public static function getSettings($pluginName)
    {
        $pluginName = $pluginName. '.';

        $objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
        $typoScriptService = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Service\TypoScriptService::class);
        $backendConfiguration = $objectManager->get(
            \TYPO3\CMS\Extbase\Configuration\BackendConfigurationManager::class
        );
        $fullTypoScript = $backendConfiguration->getTypoScriptSetup();
        if (!is_null($fullTypoScript)) {
            return $typoScriptService->convertTypoScriptArrayToPlainArray(
                $fullTypoScript['plugin.'][$pluginName]['settings.']
            );
        }
        return [];
    }
}
