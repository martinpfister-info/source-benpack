<?php

/**
 * WARNING! Order of entries in postVarsSets are important, especially for
 * 'forum' key. If you rearrange entries inside any of postVarsSets, you may end
 * up with badly formatted urls such as `/path/to/page/controller/action///some-title'.
 * Either use 'cond' where appropriate or put less used parameters last.
 */

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT'] = [
    'init' => [
        'appendMissingSlash' => 'ifNotFile,redirect',
    ],
    'fixedPostVars' => [
        '11' => [
            [
                'GETvar' => 'tx_sijobs_joboffers[action]',
                'valueMap' => [
                    'detail' => 'show',
                ],
            ],
            [
                // Limit to 'newAction' only
                'GETvar' => 'tx_sijobs_joboffers[institution]',
                'cond' => [
                    'prevValueInList' => 'new',
                ],
                'lookUpTable' => [
                    'table' => 'tx_siinstitution_domain_model_institution',
                    'id_field' => 'uid',
                    'alias_field' => 'title',
                    'addWhereClause' => ' AND deleted=0',
                    'useUniqueCache' => 1,
                    'useUniqueCache_conf' => [
                        'strtolower' => 1,
                        'spaceCharacter' => '-',
                    ],
                    'languageGetVar' => 'L',
                    'languageExceptionUids' => '',
                    'languageField' => 'sys_language_uid',
                    'transOrigPointerField' => 'l10n_parent',
                ],
            ],
            [
                // This is used in several actions. Keep it last to
                // have it empty if it is not set (for actions that do not use it)
                'GETvar' => 'tx_sijobs_joboffers[jobOffer]',
                'lookUpTable' => [
                    'table' => 'tx_sijobs_domain_model_joboffer',
                    'id_field' => 'uid',
                    'alias_field' => 'title',
                    'addWhereClause' => ' AND deleted=0',
                    'useUniqueCache' => 1,
                    'useUniqueCache_conf' => [
                        'strtolower' => 1,
                        'spaceCharacter' => '-',
                    ],
                    'languageGetVar' => 'L',
                    'languageExceptionUids' => '',
                    'languageField' => 'sys_language_uid',
                    'transOrigPointerField' => 'l10n_parent',
                ],
            ],
            [
                'GETvar' => 'tx_sijobs_joboffers[controller]',
                'noMatch' => 'bypass',
            ],
        ],
        '13' => [
            [
                'GETvar' => 'tx_sijobs_jobapplication[action]',
                'valueMap' => [
                    'detail' => 'show',
                ],
            ],
            [
                // This is used in several actions. Keep it last to
                // have it empty if it is not set (for actions that do not use it)
                'GETvar' => 'tx_sijobs_jobapplication[jobApplication]',
                'lookUpTable' => [
                    'table' => 'tx_sijobs_domain_model_jobapplication',
                    'id_field' => 'uid',
                    'alias_field' => 'concat(title, "-", uid)',
                    'addWhereClause' => ' AND deleted=0',
                    'useUniqueCache' => 1,
                    'useUniqueCache_conf' => [
                        'strtolower' => 1,
                        'spaceCharacter' => '-',
                    ],
                    'languageGetVar' => 'L',
                    'languageExceptionUids' => '',
                    'languageField' => 'sys_language_uid',
                    'transOrigPointerField' => 'l10n_parent',
                ],
            ],
            [
                'GETvar' => 'tx_sijobs_jobapplication[controller]',
                'noMatch' => 'bypass',
            ],
        ],
        '351' => [
            [
                'GETvar' => 'tx_sieducation_educationlists[action]',
                'valueMap' => [
                    'detail' => 'show',
                ],
            ],
            [
                // This is used in several actions. Keep it last to
                // have it empty if it is not set (for actions that do not use it)
                'GETvar' => 'tx_sieducation_educationlists[education]',
                'lookUpTable' => [
                    'table' => 'tx_sieducation_domain_model_education',
                    'id_field' => 'uid',
                    'alias_field' => 'concat(title, "-", uid)',
                    'addWhereClause' => ' AND deleted=0',
                    'useUniqueCache' => 1,
                    'useUniqueCache_conf' => [
                        'strtolower' => 1,
                        'spaceCharacter' => '-',
                    ],
                    'languageGetVar' => 'L',
                    'languageExceptionUids' => '',
                    'languageField' => 'sys_language_uid',
                    'transOrigPointerField' => 'l10n_parent',
                ],
            ],
            [
                'GETvar' => 'tx_sieducation_educationlists[controller]',
                'noMatch' => 'bypass',
            ],
        ],
    ],
    'postVarSets' => [
        '_DEFAULT' => [
            'news' => [
                [
                    'GETvar' => 'tx_news_pi1[news]',
                    'lookUpTable' => [
                        'table' => 'tx_news_domain_model_news',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause' => ' AND deleted=0',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                    ],
                ],
                [
                    'GETvar' => 'tx_news_pi1[action]',
                    'noMatch' => 'bypass',
                ],
                [
                    'GETvar' => 'tx_news_pi1[controller]',
                    'noMatch' => 'bypass',
                ],
            ],
            'fokuslist' => [
                'site' => [
                    'GETvar' => 'tx_siarticle_articlelist[@widget_0][currentPage]',
                ]
            ],
            'branchenratgeber' => [
                [
                    'GETvar' => 'tx_siinstitution_branchenratgeber[action]',
                    'valueMap' => [
                        'detail' => 'show',
                    ],
                ],
                [
                    'GETvar' => 'tx_siinstitution_branchenratgeber[institution]',
                    'cond' => [
                        'prevValueInList' => 'show',
                    ],
                    'lookUpTable' => [
                        'table' => 'tx_siinstitution_domain_model_institution',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause' => ' AND deleted=0',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                    ],
                ],
                [
                    'GETvar' => 'tx_siinstitution_branchenratgeber[controller]',
                    'noMatch' => 'bypass',
                ],
                'seite' => [
                    [
                        'GETvar' => 'tx_siinstitution_branchenratgeber[@widget_0][currentPage]',

                    ],
                ],
            ],
            'createrelation' => [
                [
                    'GETvar' => 'tx_siinstitution_createrelation[action]',
                    // Could have a 'valueMap' here to add better names for actions
                ],
                [
                    'GETvar' => 'tx_siinstitution_createrelation[institution]',
                    'lookUpTable' => [
                        'table' => 'tx_siinstitution_domain_model_institution',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause' => ' AND deleted=0',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                    ],
                ],
                [
                    'GETvar' => 'tx_siinstitution_createrelation[userToRemove]',
                    // Nothing to use here for a better name than uid
                ],
            ],
            'general' => [
                [
                    'GETvar' => 'tx_siinstitution_general[action]',
                    // Could have a 'valueMap' here to add better names for actions
                ],
                [
                    // This parameters exists only for some actions, so we add it
                    // Paraeters for other actions will have be normal url parameters
                    // because we cannot make config with may parameters
                    'GETvar' => 'tx_siinstitution_general[institution]',
                    'lookUpTable' => [
                        'table' => 'tx_siinstitution_domain_model_institution',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause' => ' AND deleted=0',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                    ],
                ],
            ],
            'forum' => [
                [
                    'GETvar' => 'tx_siexpertforum_forum[action]',
                ],
                [
                    'GETvar' => 'tx_siexpertforum_forum[controller]',
                    'userFunc' => 'Sozialinfo\\Base\\Utility\\RealurlHelper->convertControllerName',
                ],
                [
                    'GETvar' => 'tx_siexpertforum_forum[section]',
                    'cond' => [
                        'prevValueInList' => 'Section',
                    ],
                    'lookUpTable' => [
                        'table' => 'tx_siexpertforum_domain_model_section',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause' => ' AND deleted=0',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                    ],
                ],
                [
                    'GETvar' => 'tx_siexpertforum_forum[topic]',
                    'lookUpTable' => [
                        'table' => 'tx_siexpertforum_domain_model_topic',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'maxLength' => 50,
                        'addWhereClause' => ' AND deleted=0',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                    ],
                ],
                [
                    'GETvar' => 'tx_siexpertforum_forum[post]',
                ],
            ],
        ],
    ],
    'pagePath' => [
        'rootpage_id' => 1,
    ]
];
