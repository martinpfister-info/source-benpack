<?php
namespace Martinpfister\Base\Traits;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 sozialinfo.ch
 *  All rights reserved
 ***************************************************************/
use TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator;
use TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator;

/**
 * Class PropertyValidation
 *
 * @author Krzysztof Napora <napora@amtsolution.pl>, AMT Solution
 * @author Michał Cygankiewicz <kontakt@amtsolution.pl>, AMT Solution
 */
trait Validation
{
    /**
     * Removes standard validation from argument.property
     * https://sozialinfo.atlassian.net/browse/TNW-349
     *
     * @param string $argument object name
     * @param string $property property of the argument which standard validation should be removed
     * @return void
     */
    public function removePropertyValidation($argument, $property)
    {
        if ($this->arguments->hasArgument($argument)) {
            /** @var \TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator */
            $conjunctionValidator = $this->arguments->getArgument($argument)->getValidator();
            //get all validators for argument
            foreach ($conjunctionValidator->getValidators() as $validator) {
                if ($validator instanceof ConjunctionValidator) {
                    foreach ($validator->getValidators() as $validators) {
                        //get all validators for property
                        if ($validators instanceof GenericObjectValidator) {
                            foreach ($validators->getPropertyValidators($property) as $propertyValidator) {
                                //remove only standard validator
                                if ($propertyValidator instanceof ConjunctionValidator) {
                                    foreach ($propertyValidator->getValidators() as $valid) {
                                        $propertyValidator->removeValidator($valid);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
