<?php
namespace Martinpfister\Base\Domain\Validator;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;

class GreatherThanValidator extends AbstractValidator
{

    /**
     * @var array
     */
    protected $supportedOptions = [
        'greater' => ['', 'field one to compere', 'string'],
        'smaller' => ['', 'field two to compere', 'string']
    ];


    /**
     * configurationManager
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
     * @inject
     */
    public $configurationManager;

    /**
     * Content Object
     *
     * @var object
     */
    public $cObj;

    /**
     * Plugin Variables
     *
     * @var array
     */
    public $piVars = [];

    /**
     * TypoScript Configuration
     *
     * @var array
     */

    /**
     * Action Name
     *
     * @var string
     */
    protected $actionName;

    /**
     * Controller Name
     *
     * @var string
     */
    protected $controllerName;

    public $configuration = [];

    public function isValid($value)
    {
        $this->init();

        $field1 = $this->options['greater'];
        $field2 = $this->options['smaller'];

        $gpName =   $this->actionName.$this->controllerName;

        $field1Value = $this->piVars[$gpName][$field1];
        $field2Value = $this->piVars[$gpName][$field2];
        if ($field1Value  >  $field2Value) {
            $msg = $erroMsg = 'validationError_' . $field1 . 'can_not_be_larger_than' . $field2;
            $this->addError($msg, 'percentUp');
            return false;
        }
        return true;
    }


    /**
     * Initialize Validator Function
     *
     * @return void
     */
    protected function init()
    {
        $this->configuration = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
        );
        $this->cObj = $this->configurationManager->getContentObject();
        $this->piVars = GeneralUtility::_GP('tx_sijobs_joboffers');
        $this->actionName = $this->piVars['__referrer']['@action'];
        $this->controllerName = $this->piVars['__referrer']['@controller'];
    }

}