page.includeCSS.app = typo3conf/ext/base/Resources/Public/Template/dist/css/app.css

page.includeJS {
    jquery = typo3conf/ext/base/Resources/Public/Template/dist/js/frameworks/jquery/jquery.min.js
    bootstrap = typo3conf/ext/base/Resources/Public/Template/dist/js/frameworks/bootstrap/bootstrap.min.js
}

page.includeJSFooter {
    #mmenu = EXT:base/Resources/Public/Template/dist/js/frameworks/jQuery.mmenu/jquery.mmenu.all.js
}

page.includeJSFooter {
    base = typo3conf/ext/base/Resources/Public/Template/dist/js/app.js
}

# **********************************************************
# Google analytics (conditionally)
# **********************************************************
[globalVar = LIT:1 = {$site.googleAnalytics}]
page.headerData.100 = COA
page.headerData.100.10 = TEXT
page.headerData.100.10.value (

<script async src="https://www.googletagmanager.com/gtag/js?id={$site.googleAnalytics.account}"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', '{$site.googleAnalytics.account}');
</script>
)
[global]
