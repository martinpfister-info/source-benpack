//root namespace
var soz = soz || {};

soz.toolTip = {

    config: {
        elements: '.soz-tool-tip a'
    },

    init: function () {
        if (this.config.elements.length > 0) {
            $(this.config.elements).tooltip();
        }
    }
};
